###################
Lista della spesa
###################

Questo software gestisce la tua lista della spesa.

**************************
Requisiti
**************************

È necessario avere uno spazio web con *server Apache* e un database *MYSQL*

*******************
Installazione
*******************
Scarica l'intera repository del ramo master (produzione) e segui le istruzioni
di installazione scritte nel file /guida_installazione/index.php
