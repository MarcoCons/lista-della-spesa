-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Nov 26, 2018 alle 15:56
-- Versione del server: 10.1.34-MariaDB
-- Versione PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lista_della_spesa`
--
USE nome_database;

-- --------------------------------------------------------

--
-- Struttura della tabella `spesa_lista`
--

CREATE TABLE `spesa_lista` (
  `id` int(11) NOT NULL,
  `id_al_peso` int(11) DEFAULT NULL,
  `id_al_dettaglio` int(11) DEFAULT NULL,
  `nome_comune` varchar(150) NOT NULL,
  `marca` varchar(150) DEFAULT NULL,
  `prezzo` float DEFAULT NULL,
  `nota` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `spesa_venduti_al_dettaglio`
--

CREATE TABLE `spesa_venduti_al_dettaglio` (
  `id` int(11) NOT NULL,
  `quantita` int(11) DEFAULT '1',
  `quantita_nel_carrello` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `spesa_venduti_al_dettaglio`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `spesa_venduti_al_peso`
--

CREATE TABLE `spesa_venduti_al_peso` (
  `id` int(11) NOT NULL,
  `quantita` float DEFAULT '0',
  `unita_di_misura` varchar(4) NOT NULL,
  `quantita_nel_carrello` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `spesa_lista`
--
ALTER TABLE `spesa_lista`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `spesa_venduti_al_dettaglio`
--
ALTER TABLE `spesa_venduti_al_dettaglio`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `spesa_venduti_al_peso`
--
ALTER TABLE `spesa_venduti_al_peso`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `spesa_lista`
--
ALTER TABLE `spesa_lista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT per la tabella `spesa_venduti_al_dettaglio`
--
ALTER TABLE `spesa_venduti_al_dettaglio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `spesa_venduti_al_peso`
--
ALTER TABLE `spesa_venduti_al_peso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
