<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- highlight.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/highlight.min.js" integrity="sha256-iq71rXEe/fvjCUP9AfLY0cKudQuKAQywiUpXkRFSkLc=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/styles/atom-one-dark.min.css" integrity="sha256-akwTLZec/XAFvgYgVH1T5/369lhA2Efr22xzCNl1nHs=" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/languages/php.min.js" integrity="sha256-kQl5TmPctu+deBgPgPOss+mbKTJoOpe3LOAflXvuMUY=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/languages/sql.min.js" integrity="sha256-8R1S5jw/kayWzW0Qit+BPI6vEnDxkKs1v3HSoeiqCfY=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/languages/apache.min.js" integrity="sha256-QRFeqxcADeaTtnVntzLfpGbFTBybTeM+qKvRCGBd+R0=" crossorigin="anonymous"></script>
    <script type="text/javascript">
      hljs.initHighlightingOnLoad();
    </script>
    <title>Guida installazione</title>
  </head>
  <body>
    <div class="container my-3">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col">
          <h1 class="text-center">Guida all'installazione</h1>
          <p>
Per installare questo software è necessario uno spazio web con un server Apache
e un database MySQL.
          </p>
          <h2 class="text-center">Installazione del database</h2>
          <p>
Se hai la possibilità di creare un nuovo database utilizza il file
<code>/sql/crea_database.sql</code> altrimenti, utilizza il file
<code>/sql/crea_tabelle.sql</code> nel database a tua disposizione.
          </p>
          <p>
In questo ultimo caso, dovrai specificare il nome del database nel file
<code>/sql/crea_tabelle.sql</code>.
Sostituisci il nome del database a tua disposizione nella riga
<pre>
  <code class="sql rounded">
  --
  -- Database: `lista_della_spesa`
  --
  USE nome_database;
  </code>
</pre>
          </p>
          <p>
Una volta capito quale file dovrai utilizzare, copia l'intero file e eseguilo
nel tuo gestore database (ad es. PhpMyAdmin) come puro codice SQL. Verrà creato
il database necessario a far funzionare il software.
          </p>
          <h2 class="text-center">Configurazione</h2>
          <p>
Prima di installare il software, va configurato modificando i file in locale.
          </p>
          <p>
Quando hai deciso in che cartella installare il software in remoto, modifica il
file <code>/application/config/production/config.php</code>, sostituendo il dominio
con la sottocartella dove installerai il software. Assicurati di utilizzare uno
slash finale.
<pre>
  <code class="rounded php">
    /*
    |--------------------------------------------------------------------------
    | Base Site URL
    |--------------------------------------------------------------------------
    |
    | URL to your CodeIgniter root. Typically this will be your base URL,
    | WITH a trailing slash:
    |
    |	http://example.com/
    |
    | WARNING: You MUST set this value!
    |
    | If it is not set, then CodeIgniter will try guess the protocol and path
    | your installation, but due to security concerns the hostname will be set
    | to $_SERVER['SERVER_ADDR'] if available, or localhost otherwise.
    | The auto-detection mechanism exists only for convenience during
    | development and MUST NOT be used in production!
    |
    | If you need to allow multiple domains, remember that this file is still
    | a PHP script and you can easily do that on your own.
    |
    */
    $config['base_url'] = 'http://dominio.it/spesa/';
  </code>
</pre>
          </p>
          <p>
Nel file <code>/application/config/production/database.php</code> configurerai il
database. Modifica le impostazioni <code>hostname</code>, <code>username</code>,
<code>password</code> e <code>database</code> con i valori giusti del database
che utilizzerai.
<pre>
  <code class="rounded php">
    $db['default'] = array(
    	'dsn'	=> '',
    	'hostname' => 'localhost',
    	'username' => 'utente',
    	'password' => 'password',
    	'database' => 'nome_database',
    	'dbdriver' => 'mysqli',
    	'dbprefix' => 'spesa_',
    	'pconnect' => FALSE,
    	'db_debug' => (ENVIRONMENT !== 'production'),
    	'cache_on' => FALSE,
    	'cachedir' => '',
    	'char_set' => 'utf8',
    	'dbcollat' => 'utf8_general_ci',
    	'swap_pre' => '',
    	'encrypt' => FALSE,
    	'compress' => FALSE,
    	'stricton' => FALSE,
    	'failover' => array(),
    	'save_queries' => TRUE
    );
  </code>
</pre>
          </p>
          <p>
Infine, apri il file <code>/.htaccess</code> e modifica la riga <code>RewriteBase</code>
con la sottocartella (o le sottocartelle) che hai specificato nella variabile
<code>$config['base_url']</code>. Assicurati che abbia uno slash iniziale e finale.
<pre>
  <code class="rounded apache">
    RewriteBase /sottocartella/
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule ^(.*)$ index.php?/$1 [L]
  </code>
</pre>
          </p>
          <h2 class="text-center">Caricamento tramite FTP</h2>
          <p>
Adesso il software è configurato e va caricato nella cartella specificata nella
variabile <code>$config['base_url']</code>.
          </p>
          <p>
Una volta fatto, puoi cominciare a utilizzare il software!
          </p>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
 </html>
