<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Rappresenta un bene venduto al dettaglio, come ad esempio una confezione di
 * pasta.
 */
class Bene_venduto_al_dettaglio extends Vendita_al_dettaglio
{
  /**
   * ID del record nel database.
   * @var int
   */
  private $id;

  /**
   * Getter per $id.
   * @return int L'ID di questo oggetto.
   */
  public function get_id()
  {
    return $this->id;
  }

  /**
   * Setter per $id.
   * @param int $id L'ID da attribuire a questa istanza.
   * @return Bene_venduto_al_dettaglio Questa istanza. Permette una catena di setter.
   */
  public function set_id(int $id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * ID specifico nel record del database.
   * @var [type]
   */
  private $id_specifico;

  /**
   * Getter per $id_specifico
   * @return int L'ID specifico di questa istanza.
   */
  public function get_id_specifico()
  {
    return $this->id_specifico;
  }

  /**
   * Setter per $id_specifico.
   * @param  int    $id L'ID specifico da attribuire a questa istanza.
   * @return Bene_venduto_al_dettaglio Questa istanza. Permette una catena di setter.
   */
  public function set_id_specifico(int $id)
  {
    $this->id_specifico = $id;
    return $this;
  }

  /**
   * Nome comune del bene da rappresentare.
   * @var string
   */
  private $nome_comune;

  /**
   * Getter per $nome_comune.
   * @return string Il nome comune di questa istanza.
   */
  public function get_nome_comune()
  {
    return $this->nome_comune;
  }

  /**
   * Setter per $nome_comune.
   * @param  string $nome Il nome
   * @return Bene_venduto_al_dettaglio Questa istanza. Permette una catena di setter.
   */
  public function set_nome_comune(string $nome)
  {
    $this->nome_comune = $nome;
    return $this;
  }

  /**
   * Nome della marca, nel caso si voglia rappresentare un bene specifico.
   * @var string|null
   */
  private $marca;

  /**
   * Getter per $marca;
   * @return string|null La marca di questa istanza.
   */
  public function get_marca()
  {
    return $this->marca;
  }

  /**
   * Setter per $marca.
   * @param  string $nome_marca Il nome della marca da attribuire a questa istanza.
   * @return Bene_venduto_al_dettaglio Questa istanza. Permette una catena di setter.
   */
  public function set_marca(string $nome_marca)
  {
    $this->marca = $nome_marca;
    return $this;
  }

  /**
   * Prezzo del bene da comprare, indipendentemente dal tipo di vendita (al
   * peso o al dettaglio).
   * @var float
   */
  private $prezzo;

  /**
   * Getter per $prezzo.
   * @return float|null Il prezzo di questa istanza.
   */
  public function get_prezzo()
  {
    return $this->prezzo;
  }

  /**
   * Setter per $prezzo.
   * @param  float|null $prezzo Il prezzo da attribuire a questa istanza.
   * @return Bene_venduto_al_dettaglio Questa istanza. Permette una catena di setter.
   */
  public function set_prezzo(float $prezzo)
  {
    $this->prezzo = $prezzo;
    return $this;
  }

  /**
   * Nota testuale di utilizzo per l'utente, nel caso volesse commentare il bene
   * da comprare.
   * @var string
   */
  private $nota;

  /**
   * Getter per $nota;
   * @return string|null La nota di questa istanza.
   */
  public function get_nota()
  {
    return $this->nota;
  }

  /**
   * Setter per $nota.
   * @param  string $testo La nota da attribuire a questa istanza.
   * @return Bene_venduto_al_dettaglio Questa istanza. Permette una catena di setter.
   */
  public function set_nota(string $testo)
  {
    $this->nota = $testo;
    return $this;
  }

  /**
   * Il costruttore vuoto.
   */
  public function __construct()
  {

  }
}


/**
 * Definisce quei beni che vengono venduti al dettaglio.
 */
abstract class Vendita_al_dettaglio
{
  /**
   * Pezzi o confezioni da comprare del bene venduto al dettaglio.
   * @var int
   */
  private $pezzi;

  /**
   * Getter per $pezzi
   * @return int|null Il numero di pezzi di questa istanza.
   */
  public function get_quantità()
  {
    return $this->pezzi;
  }

  /**
   * Setter per $pezzi
   * @param int $numero_pezzi I pezzi da attribuire a questa istanza.
   * @return Bene_venduto_al_dettaglio Questa istanza. Permette una catena di setter.
   */
  public function set_quantità(int $numero_pezzi)
  {
    $this->pezzi = $numero_pezzi;
    return $this;
  }

  /**
   * Pezzi o confezioni effettivamente comprarete.
   * @var int
   */
  private $pezzi_nel_carrello;

  /**
   * Getter per $pezzi_nel_carrello
   * @return int|null Il numero di pezzi nel carrello di questa istanza.
   */
  public function get_quantità_nel_carrello()
  {
    return $this->pezzi_nel_carrello;
  }

  /**
   * Setter per $pezzi_nel_carrello
   * @param int $numero_pezzi I pezzi nel carrello da attribuire a questa istanza.
   * @return Bene_venduto_al_dettaglio Questa istanza. Permette una catena di setter.
   */
  public function set_quantità_nel_carrello(int $numero_pezzi)
  {
    $this->pezzi_nel_carrello = $numero_pezzi;
    return $this;
  }
}
