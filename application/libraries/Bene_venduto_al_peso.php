<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Rappresenta un bene venduto al peso, come ad esempio 2 etti di prosciutto.
 */
class Bene_venduto_al_peso extends Vendita_al_peso
{
  /**
   * ID nella tabella 'lista'.
   * @var int
   */
  private $id;

  /**
   * Getter per $id.
   * @return int L'ID di questo oggetto.
   */
  public function get_id()
  {
    return $this->id;
  }

  /**
   * Setter per $id.
   * @param int $id L'ID da attribuire a questa istanza.
   * @return Bene_venduto_al_peso Questa istanza. Permette una catena di setter.
   */
  public function set_id(int $id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * ID nella tabella 'elementi_a_peso'.
   * @var int
   */
  private $id_specifico;

  /**
   * Getter per $id_specifico
   * @return int L'ID specifico di questa istanza.
   */
  public function get_id_specifico()
  {
    return $this->id_specifico;
  }

  /**
   * Setter per $id_specifico.
   * @param  int    $id L'ID specifico da attribuire a questa istanza.
   * @return Bene_venduto_al_peso Questa istanza. Permette una catena di setter.
   */
  public function set_id_specifico(int $id)
  {
    $this->id_specifico = $id;
    return $this;
  }

  /**
   * Nome comune del bene da rappresentare.
   * @var string
   */
  private $nome_comune;

  /**
   * Getter per $nome_comune.
   * @return string Il nome comune di questa istanza.
   */
  public function get_nome_comune()
  {
    return $this->nome_comune;
  }

  /**
   * Setter per $nome_comune.
   * @param  string $nome Il nome
   * @return Bene_venduto_al_peso Questa istanza. Permette una catena di setter.
   */
  public function set_nome_comune(string $nome)
  {
    $this->nome_comune = $nome;
    return $this;
  }

  /**
   * Nome della marca, nel caso si voglia rappresentare un bene specifico.
   * @var string|null
   */
  private $marca;

  /**
   * Getter per $marca;
   * @return string|null La marca di questa istanza.
   */
  public function get_marca()
  {
    return $this->marca;
  }

  /**
   * Setter per $marca.
   * @param  string $nome_marca Il nome della marca da attribuire a questa istanza.
   * @return Bene_venduto_al_peso Questa istanza. Permette una catena di setter.
   */
  public function set_marca($nome_marca)
  {
    $this->marca = $nome_marca;
    return $this;
  }

  /**
   * Prezzo del bene da comprare, indipendentemente dal tipo di vendita (al
   * peso o al dettaglio).
   * @var float|null
   */
  private $prezzo;

  /**
   * Getter per $prezzo.
   * @return float|null Il prezzo di questa istanza.
   */
  public function get_prezzo()
  {
    return $this->prezzo;
  }

  /**
   * Setter per $prezzo.
   * @param  float|null $prezzo Il prezzo da attribuire a questa istanza.
   * @return Bene_venduto_al_peso Questa istanza. Permette una catena di setter.
   */
  public function set_prezzo(float $prezzo)
  {
    $this->prezzo = $prezzo;
    return $this;
  }

  /**
   * Nota testuale di utilizzo per l'utente, nel caso volesse commentare il bene
   * da comprare.
   * @var string|null
   */
  private $nota;

  /**
   * Getter per $nota;
   * @return string|null La nota di questa istanza.
   */
  public function get_nota()
  {
    return $this->nota;
  }

  /**
   * Setter per $nota.
   * @param  string $testo La nota da attribuire a questa istanza.
   * @return Bene_venduto_al_peso Questa istanza. Permette una catena di setter.
   */
  public function set_nota($testo)
  {
    $this->nota = $testo;
    return $this;
  }

  /**
   * Il costruttore vuoto.
   */
  public function __construct()
  {

  }
}

/**
 * Definisce quei beni che vengono venduti al peso.
 */
abstract class Vendita_al_peso
{
  /**
   * Quantità del bene da comprare espressa in peso.
   * @property double $peso
   * @var float|null
   */
  private $peso;

  /**
   * Getter per $peso.
   * @return float|null Il peso di questa istanza.
   */
  public function get_quantità()
  {
    return $this->peso;
  }

  /**
   * Setter per $peso.
   * @param  float $peso Il peso da attribuire a questa istanza.
   * @return Bene_venduto_al_peso Questa istanza. Permette una catena di setter.
   */
  public function set_quantità(float $peso)
  {
    $this->peso = $peso;
    return $this;
  }

  /**
   * Unità di misura con cui si intende misurare la quantità del bene da comprare.
   * @var string
   */
  private $misura;

  /**
   * Getter per $misura.
   * @return string L'unità di misura di questa istanza.
   */
  public function get_misura()
  {
    return $this->misura;
  }

  /**
   * Setter per $misura;
   * @param  string $unità L'unità di misura da attribuire a questa istanza.
   * @return Bene_venduto_al_peso Questa istanza. Permette una catena di setter.
   */
  public function set_misura(string $unità)
  {
    $this->misura = $unità;
    return $this;
  }

  /**
   * Peso effettivamente acquistato del bene.
   * @var float|null
   */
  private $peso_nel_carrello;

  /**
   * Getter per $peso.
   * @return float|null Il peso effettivamente nel carrello di questa istanza.
   */
  public function get_quantità_nel_carrello()
  {
    return $this->peso_nel_carrello;
  }

  /**
   * Setter per $peso.
   * @param  float $peso Il peso nel carrello da attribuire a questa istanza.
   * @return Bene_venduto_al_peso Questa istanza. Permette una catena di setter.
   */
  public function set_quantità_nel_carrello(float $peso)
  {
    $this->peso_nel_carrello = $peso;
    return $this;
  }
}
