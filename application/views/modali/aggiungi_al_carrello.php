<?php foreach($lista as $bene): ?>
  <?php if(get_class($bene) == "Bene_venduto_al_dettaglio")
          $tipo = "al_dettaglio";
  ?>
  <?php if(get_class($bene) == "Bene_venduto_al_peso")
          $tipo = "al_peso";
  ?>

<!-- Modale aggiungi nel carrello -->
<div class="modal fade" id="aggiungi_al_carrello_<?=$bene->get_id()?>" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Aggiungi nel carrello</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
<?php if($tipo == "al_dettaglio"): ?>
      <form method="post" action="<?=base_url()?>lista/aggiungi_al_carrello_al_dettaglio">
<?php elseif($tipo == "al_peso"): ?>
      <form method="post" action="<?=base_url()?>lista/aggiungi_al_carrello_al_peso">
<?php endif; ?>
<?php if($tipo == "al_dettaglio"): ?>
        <div class="modal-body">
          <label for="quantità_nel_carrello">Quantità nel carrello</label>
          <input class="form-control" id="quantità_nel_carrello" type="number" name="nel_carrello" value="<?=$bene->get_quantità()?>">
        </div>
        <input type="number" name="id" value="<?=$bene->get_id()?>" hidden>
        <input type="number" name="id_specifico" value="<?=$bene->get_id_specifico()?>" hidden>
        <input type="text" name="nome_comune" value="<?=$bene->get_nome_comune()?>" hidden>
        <input type="text" name="marca" value="<?=$bene->get_marca()?>" hidden>
        <input type="text" name="prezzo" value="<?=$bene->get_prezzo()?>" hidden>
        <input type="text" name="quantità" value="<?=$bene->get_quantità()?>" hidden>
        <input type="text" name="nota" value="<?=$bene->get_nota()?>" hidden>
<?php elseif($tipo == "al_peso"): ?>
        <div class="modal-body">
          <label for="quantità_nel_carrello">Quantità nel carrello</label>
          <div class="input-group mb-3">
            <input class="form-control" id="quantità_nel_carrello" type="number" name="nel_carrello" value="<?=$bene->get_quantità()?>">
            <div class="input-group-append">
              <span class="input-group-text" id="basic-addon2"><?=$bene->get_misura()?></span>
            </div>
          </div>
        </div>
        <input type="number" name="id" value="<?=$bene->get_id()?>" hidden>
        <input type="number" name="id_specifico" value="<?=$bene->get_id_specifico()?>" hidden>
        <input type="text" name="nome_comune" value="<?=$bene->get_nome_comune()?>" hidden>
        <input type="text" name="marca" value="<?=$bene->get_marca()?>" hidden>
        <input type="number" step="0.01" name="prezzo" value="<?=$bene->get_prezzo()?>" hidden>
        <input type="number" step="0.1" name="quantità" value="<?=$bene->get_quantità()?>" hidden>
        <input type="text" name="misura" value="<?=$bene->get_misura()?>" hidden>
        <input type="text" name="nota" value="<?=$bene->get_nota()?>" hidden>
<?php endif; ?>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Aggiungi</button>
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Annulla</button>
          </div>
      </form>
    </div><!-- .modal-content -->
  </div><!-- .modal-dialog -->
</div><!-- .modal -->

<?php endforeach; ?>
