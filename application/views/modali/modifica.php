<?php foreach($lista as $bene): ?>
  <!-- Modale #modifica -->
  <?php if(get_class($bene) == "Bene_venduto_al_dettaglio")
          $tipo = "al_dettaglio";
  ?>
  <?php if(get_class($bene) == "Bene_venduto_al_peso")
          $tipo = "al_peso";
  ?>
<div class="modal fade" id="modifica_<?=$bene->get_id()?>" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modifica</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?=base_url()?>lista/modifica_<?=$tipo?>">
        <div class="modal-body">
  <?php if($tipo == "al_dettaglio"): ?>
          <div class="form-group">
            <label for="nome_comune">Nome comune</label>
            <input class="form-control" id="nome_comune" type="text" name="nome_comune" value="<?=$bene->get_nome_comune()?>">
          </div>
          <div class="form-group">
            <label for="marca">Marca</label>
            <input class="form-control" id="marca" type="text" name="marca" value="<?=$bene->get_marca()?>">
          </div>
          <div class="form-group">
            <label for="prezzo">Prezzo</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">€</span>
              </div>
              <input type="number" name="prezzo" id="prezzo" step="0.01" class="form-control" value="<?=$bene->get_prezzo()?>">
            </div>
          </div>
          <div class="form-group">
            <label for="quantità">Quantità</label>
            <input type="number" class="form-control" id="quantità" step="1" name="quantità" value="<?=$bene->get_quantità()?>">
          </div>
          <div class="form-group">
            <label for="nota">Nota</label>
            <textarea class="form-control" type="text" id="nota" name="nota" row="3"><?=$bene->get_nota()?></textarea>
          <input type="number" name="id" value="<?=$bene->get_id()?>" hidden>
          <input type="number" name="id_specifico" value="<?=$bene->get_id_specifico()?>" hidden>
  <?php elseif($tipo == "al_peso"): ?>
          <div class="form-group">
            <label for="nome_comune">Nome comune</label>
            <input class="form-control" id="nome_comune" type="text" name="nome_comune" value="<?=$bene->get_nome_comune()?>">
          </div>
          <div class="form-group">
            <label for="marca">Marca</label>
            <input class="form-control" id="marca" type="text" name="marca" value="<?=$bene->get_marca()?>">
          </div>
          <div class="form-group">
            <label for="prezzo">Prezzo</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">€</span>
              </div>
              <input type="number" name="prezzo" id="prezzo" step="0.01" class="form-control" value="<?=$bene->get_prezzo()?>">
            </div>
          </div>
          <div class="form-group">
            <label for="quantità">Quantità</label>
            <div class="input-group mb-3">
              <input class="form-control" type="number" step="0.1" class="form-control" name="quantità" value="<?=$bene->get_quantità()?>">
              <div class="input-group-append">
                <span class="input-group-text"><?=$bene->get_misura()?></span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="misura">Unità di misura</label>
            <input class="form-control" type="text" id="misura" maxlength="4" name="misura" value="<?=$bene->get_misura()?>">
          </div>
          <div class="form-group">
            <label for="nota">Nota</label>
            <textarea class="form-control" type="text" id="nota" name="nota" row="3"><?=$bene->get_nota()?></textarea>
          </div>
          <input type="number" name="id" value="<?=$bene->get_id()?>" hidden>
          <input type="number" name="id_specifico" value="<?=$bene->get_id_specifico()?>" hidden>
  <?php endif; ?>
        </div><!-- .modal-body -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-warning text-white">Modifica</button>
          <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Annulla</button>
        </div>
      </form>
      </div><!-- .modal-body -->
    </div><!-- .modal-content -->
  </div><!-- .modal-dialog -->
</div><!-- .modal -->


<?php endforeach; ?>
