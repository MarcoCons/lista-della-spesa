<?php foreach($lista as $bene): ?>
  <?php if(get_class($bene) == "Bene_venduto_al_dettaglio")
          $tipo = "al_dettaglio";
  ?>
  <?php if(get_class($bene) == "Bene_venduto_al_peso")
          $tipo = "al_peso";
  ?>
  <!-- Modale #elimina -->
<div class="modal fade" id="elimina_<?=$bene->get_id()?>" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Elimina</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?=base_url()?>lista/elimina/<?=$bene->get_id()?>/<?=$bene->get_id_specifico()?>/<?=$tipo?>">
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger">Elimina</button>
          <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Annulla</button>
        </div>
      </form>
    </div><!-- .modal-content -->
  </div><!-- .modal-dialog -->
</div><!-- .modal -->

<?php endforeach; ?>
