<!-- Modale crea nuovo elemento -->
<div class="modal fade" id="aggiungi_nuovo" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Aggiungi</h5>
        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <button id="pulsante_aggiungi_al_dettaglio" class="btn btn-primary mt-3 w-75 mx-auto" type="button" data-toggle="collapse" data-target="#aggiungi_al_dettaglio" aria-expanded="false" aria-controls="multiCollapseExample2">Bene venduto al dettaglio</button>
      <button id="pulsante_aggiungi_al_peso" class="btn btn-primary my-3 w-75 mx-auto" type="button" data-toggle="collapse" data-target="#aggiungi_al_peso" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2">Bene venduto al peso</button>
      <div id="forms">
        <!-- Beni venduti al dettaglio -->
        <div class="collapse multiple-collapse" data-parent="#forms" id="aggiungi_al_dettaglio">
          <form method="post" action="<?=base_url()?>lista/aggiungi_al_dettaglio">
            <div class="modal-body">
              <div class="form-group">
                <label for="nome_comune">Nome comune</label>
                <input class="form-control" id="nome_comune" type="text" name="nome_comune">
              </div>
              <div class="form-group">
                <label for="marca">Marca</label>
                <input class="form-control" id="marca" type="text" name="marca">
              </div>
              <div class="form-group">
                <label for="prezzo">Prezzo</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">€</span>
                  </div>
                  <input type="number" name="prezzo" id="prezzo" step="0.01" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="quantità">Quantità</label>
                <input type="number" value="1" class="form-control" id="quantità" step="1" name="quantità">
              </div>
              <div class="form-group">
                <label for="nota">Nota</label>
                <textarea class="form-control" name="nota" rows="3" class="form-control" id="nota"></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Annulla</button>
              <button type="submit" class="btn btn-success">Aggiungi</button>
            </div>
          </form>
        </div><!-- .collapse -->

        <!-- Beni venduti al peso -->
        <div class="collapse multiple-collapse" data-parent="#forms" id="aggiungi_al_peso">
          <form method="post" action="<?=base_url()?>lista/aggiungi_al_peso">
            <div class="modal-body">
              <div class="form-group">
                <label for="nome_comune">Nome comune</label>
                <input class="form-control" id="nome_comune" type="text" name="nome_comune">
              </div>
              <div class="form-group">
                <label for="marca">Marca</label>
                <input class="form-control" id="marca" type="text" name="marca">
              </div>
              <div class="form-group">
                <label for="prezzo">Prezzo</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">€</span>
                  </div>
                  <input type="number" name="prezzo" id="prezzo" step="0.01" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="quantità">Quantità</label>
                <input type="number" class="form-control" id="quantità" step="0.1" name="quantità">
              </div>
              <div class="form-group">
                <label for="misura">Unità di misura</label>
                <input class="form-control" type="text" id="misura" maxlength="4" name="misura">
              </div>
              <div class="form-group">
                <label for="nota">Nota</label>
                <textarea name="nota" rows="3" class="form-control" id="nota"></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Annulla</button>
              <button type="submit" class="btn btn-success">Aggiungi</button>
            </div>
          </form>
        </div><!-- .collapse -->
      </div><!-- #forms -->
    </div><!-- .modal-content -->
  </div><!-- .modal-dialog -->
</div><!-- .modal -->
