<!-- Modale svuota lista -->
<div class="modal fade" id="svuota_lista" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Sicuro di voler cancellare tutto?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <form method="post" action="<?=base_url()?>lista/svuota">
          <button type="submit" class="btn btn-outline-danger">Cancella tutto</button>
        </form>
        <button type="button" class="btn btn-success mx-5" data-dismiss="modal">No!</button>
      </div><!-- .modal-footer -->
    </div><!-- .modal-content -->
  </div><!-- .modal-dialog -->
</div><!-- .modal -->
