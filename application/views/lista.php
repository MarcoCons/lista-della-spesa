<div class="container-fluid">
  <div class="row">
    <div class="col-xl-2"></div>
    <div class="col">
      <div class="tabella-responsive">
        <div class="intestazione-tabella rounded ombra sticky-top font-weight-bold d-flex flex-column flex-sm-row justify-content-md-between justify-content-xl-start py-2">
          <div class="d-flex align-items-center pb-2 terna">
            <div class="nome-comune">
              Nome comune
            </div>
            <div class="marca">
              Marca
            </div>
            <div class="prezzo">
              Prezzo
            </div>
          </div><!-- wrapper -->
          <div class="d-flex align-items-center pb-2 terna">
            <div class="quantità">
              Quantità
            </div>
            <div class="carrello">
              Nel carrello
            </div>
            <div class="note">
              Note
            </div>
          </div><!-- wrapper -->
          <div class="spazio-extra-intestazione"></div>
        </div><!-- .intestazione-tabella -->
        <div class="corpo-tabella text-center my-2">
<?php foreach($lista as $bene): ?>
  <?php if(get_class($bene) == "Bene_venduto_al_dettaglio")
          $tipo = "al_dettaglio";
        if(get_class($bene) == "Bene_venduto_al_peso")
          $tipo = "al_peso";
  ?>
  <?php $nel_carrello = (bool) $bene->get_quantità_nel_carrello()?>
          <div class="d-xl-flex riga-intera <?=!$nel_carrello ? : 'bg-light'?> pt-xl-2 align-items-xl-center">
            <div class="d-sm-flex riga py-2 <?=!$nel_carrello ? : 'bg-light'?>">
              <div class="d-flex align-items-center pb-2 terna">
                <div class="nome-comune">
                  <?=$bene->get_nome_comune()?>
                </div>
                <div class="marca">
                  <?=$bene->get_marca()?>
                </div>
                <div class="prezzo">
  <?php if($bene->get_prezzo()): ?>
                € <?=formatta_prezzo((float)$bene->get_prezzo())?>
  <?php endif; ?>
                </div>
              </div><!-- wrapper -->
              <div class="d-flex align-items-center pb-2 terna">
                <div class="quantità">
  <?php if($tipo == "al_dettaglio"): ?>
                  <?=$bene->get_quantità()?>
  <?php elseif($tipo == "al_peso"): ?>
    <?php if($bene->get_quantità()): ?>
                  <?=$bene->get_quantità()?> <?=$bene->get_misura()?>
    <?php endif; ?>
  <?php endif; ?>
                </div>
                <div class="carrello">
  <?php if($tipo == "al_dettaglio"): ?>
                  <?=$bene->get_quantità_nel_carrello()?>
  <?php elseif($tipo == "al_peso"): ?>
    <?php if($bene->get_quantità_nel_carrello()): ?>
                  <?=$bene->get_quantità_nel_carrello()?> <?=$bene->get_misura()?>
    <?php endif; ?>
  <?php endif; ?>
                </div>
                <div class="note">
                  <small><?=$bene->get_nota()?></small>
                </div>
              </div><!-- wrapper -->
            </div><!-- .riga -->
            <div class="pulsanti pb-2 border-bottom <?=!$nel_carrello ? : 'bg-light'?>">
              <div class="contenitore-pulsanti mx-auto d-flex justify-content-around">
                <button type="button" class="btn btn-danger py-0 px-2" data-toggle="modal" data-target="#elimina_<?=$bene->get_id()?>">
                  <i class="fas fa-times"></i>
                </button>
                <button type="button" class="btn btn-warning py-0 px-1" data-toggle="modal" data-target="#modifica_<?=$bene->get_id()?>">
                  <i class="fas fa-pencil-alt text-white"></i>
                </button>
                <button type="button" class="btn btn-success py-0 px-1" data-toggle="modal" data-target="#aggiungi_al_carrello_<?=$bene->get_id()?>">
                  <i class="fas fa-cart-arrow-down"></i>
                </button>
              </div><!-- .contenitore-pulsanti -->
            </div><!-- .pulsanti -->
          </div>
<?php endforeach; ?>
        </div><!-- .corpo-tabella -->
      </div><!-- .tabella-responsive -->

      <div class="alert alert-secondary my-3">
        <h4 class="">Totale: </h4>
        <h4 class="display-4 d-inline">€ <?=$totale[0]?></h4>
        <p class="d-inline font-weight-light" style="font-size: 2.6rem">,<?=$totale[1]?></p>
      </div>

      <div class="text-center">
        <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#svuota_lista">Svuota lista</button>
      </div>

    </div><!-- .col -->
    <div class="col-xl-2"></div>
  </div><!-- .row -->
</div><!-- .container-fluid -->

<div class="pulsante-aggiungi" data-toggle="modal" data-target="#aggiungi_nuovo">
  <i class="fas fa-2x fa-plus-circle text-success"></i>
</div>
