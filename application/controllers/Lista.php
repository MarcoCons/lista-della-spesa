<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lista extends CI_Controller
{
  /**
   * Carica l'intera lista della spesa e la visualizza nella pagina principale.
   */
  public function index()
  {
    // Carica l'intera lista della spesa.
    $lista_della_spesa = $this->spesa->lista_intera();
    $dati['lista'] = $lista_della_spesa;

    // Calcola il totale.
    $totale = formatta_prezzo((float)$this->spesa->totale());
    $totale = explode(',', $totale);
    $dati['totale'] = $totale;

    // Carica l'interfaccia grafica.
    $this->load->view('header');
    $this->load->view('modali/elimina', $dati);
    $this->load->view('modali/modifica', $dati);
    $this->load->view('modali/aggiungi_al_carrello', $dati);
    $this->load->view('modali/aggiungi_nuovo');
    $this->load->view('modali/svuota_lista');
    $this->load->view('lista.php', $dati);
    $this->load->view('footer.php');
  }

  /**
   * Elimina definitivamente un bene dalla lista della spesa.
   * @param  int    $id_generico  ID della tabella 'lista'.
   * @param  int    $id_specifico ID della tabella 'venduti_al_dettaglio' o 'venduti_al_peso'.
   * @param  string $tipo         Tipo del bene da eliminare: 'al_dettaglio' o 'al_peso'.
   */
  public function elimina(int $id_generico, int $id_specifico, string $tipo)
  {
    if($tipo == "al_dettaglio")
    {
      $bene = new Bene_venduto_al_dettaglio();
      $bene->set_id($id_generico)
           ->set_id_specifico($id_specifico);
      $this->spesa->elimina_al_dettaglio($bene);
    }
    elseif($tipo == "al_peso")
    {
      $bene = new Bene_venduto_al_peso();
      $bene->set_id($id_generico)
           ->set_id_specifico($id_specifico);
      $this->spesa->elimina_al_peso($bene);
    }
    redirect("/");
  }

  /**
   * Modifica un bene venduto al dettaglio presente nella lista.
   */
  public function modifica_al_dettaglio()
  {
    $bene = new Bene_venduto_al_dettaglio();
    $bene->set_id($this->input->post('id'))
         ->set_id_specifico($this->input->post('id_specifico'))
         ->set_nome_comune($this->input->post('nome_comune'))
         ->set_marca($this->input->post('marca'))
         ->set_prezzo($this->input->post('prezzo'))
         ->set_quantità($this->input->post('quantità'))
         ->set_quantità_nel_carrello((int)$this->input->post('nel_carrello'))
         ->set_nota($this->input->post('nota'));

    $this->spesa->modifica_al_dettaglio($bene);

    redirect('/');
  }

  /**
   * Modifica un bene venduto al peso presente nella lista.
   */
  public function modifica_al_peso()
  {
    $bene = new Bene_venduto_al_peso();
    $bene->set_id($this->input->post('id'))
         ->set_id_specifico($this->input->post('id_specifico'))
         ->set_nome_comune($this->input->post('nome_comune'))
         ->set_marca($this->input->post('marca'))
         ->set_prezzo($this->input->post('prezzo'))
         ->set_quantità($this->input->post('quantità'))
         ->set_misura($this->input->post('misura'))
         ->set_quantità_nel_carrello((float)$this->input->post('nel_carrello'))
         ->set_nota($this->input->post('nota'));

    $this->spesa->modifica_al_peso($bene);

    redirect('/');
  }

  /**
   * Aggiunge un bene venduto al dettaglio nel carrello.
   */
  public function aggiungi_al_carrello_al_dettaglio()
  {
    $id_specifico = $this->input->post('id_specifico');
    $quantità_nel_carrello = $this->input->post('nel_carrello');
    $this->spesa->aggiungi_al_carrello_al_dettaglio($id_specifico,$quantità_nel_carrello);

    redirect('/');
  }

  /**
   * Aggiunge un bene venduto al peso nel carrello.
   */
  public function aggiungi_al_carrello_al_peso()
  {
    $id_specifico = $this->input->post('id_specifico');
    $quantità_nel_carrello = $this->input->post('nel_carrello');
    $this->spesa->aggiungi_al_carrello_al_peso($id_specifico,$quantità_nel_carrello);

    redirect('/');
  }

  /**
   * Aggiunge un nuovo bene venduto al dettaglio nella lista.
   */
  public function aggiungi_al_dettaglio()
  {
    $bene = new Bene_venduto_al_dettaglio();
    $bene->set_nome_comune($this->input->post('nome_comune'))
         ->set_marca($this->input->post('marca'))
         ->set_prezzo((float)$this->input->post('prezzo'))
         ->set_quantità($this->input->post('quantità'))
         ->set_quantità_nel_carrello((int)$this->input->post('nel_carrello'))
         ->set_nota($this->input->post('nota'));

    $this->spesa->aggiungi_al_dettaglio($bene);

    redirect('/');
  }

  /**
   * Aggiunge un nuovo bene venduto al peso nella lista.
   */
  public function aggiungi_al_peso()
  {
    $bene = new Bene_venduto_al_peso();
    $bene->set_nome_comune($this->input->post('nome_comune'))
         ->set_marca($this->input->post('marca'))
         ->set_prezzo((float)$this->input->post('prezzo'))
         ->set_quantità((float)$this->input->post('quantità'))
         ->set_misura($this->input->post('misura'))
         ->set_quantità_nel_carrello((float)$this->input->post('nel_carrello'))
        ->set_nota($this->input->post('nota'));

    $this->spesa->aggiungi_al_peso($bene);

    redirect('/');
  }

  public function svuota()
  {
    $this->spesa->svuota_lista();

    redirect('/');
  }
}
