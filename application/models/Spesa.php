<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Il modello Spesa si occupa di implementare le operazioni CRUD sulla lista
 * della spesa.
 */
class Spesa extends CI_Model
{
  /**
   * Aggiunge nella lista della spesa un bene venduto al peso.
   * @param  Bene_venduto_al_peso $bene Il bene che si vuole inserire nella
   * lista della spesa.
   * @return Bene_venduto_al_peso|false Lo stesso oggetto passato in input ma con gli ID aggiornati.
   * Se fallisce una delle due registrazioni (generica o specifica) restituisce
   * false.
   */
  public function aggiungi_al_peso(Bene_venduto_al_peso $bene)
  {
    // Prepara i dati specifici per i beni venduti al peso.
    $dati_specifici = array(
      'quantita' => $bene->get_quantità(),
      'unita_di_misura' => $bene->get_misura(),
      'quantita_nel_carrello' => $bene->get_quantità_nel_carrello()
    );

    // Esegue la query e prende l'ID specifico autogenerato dal database.
    $aggiunto_dati_specifici = $this->db->insert($this->db->dbprefix('venduti_al_peso'), $dati_specifici, true);
    $bene->set_id_specifico($this->db->insert_id());

    // Ottiene l'istruzione SQL per il debug.
    $SQL_specifica = $this->db->last_query();

    // Prepara i dati generici comuni a tutti i beni.
    $dati_generici = array(
      'id_al_peso' => $bene->get_id_specifico(),
      'id_al_dettaglio' => null,
      'nome_comune' => $bene->get_nome_comune(),
      'marca' => $bene->get_marca(),
      'prezzo' => $bene->get_prezzo(),
      'nota' => $bene->get_nota()
    );

    // Esegue la query e prende l'ID generico autogenerato dal database.
    $aggiunto_dati_generici = $this->db->insert($this->db->dbprefix('lista'), $dati_generici, true);
    $bene->set_id($this->db->insert_id());

    // Ottiene l'istruzione SQL per il debug.
    $SQL_generica = $this->db->last_query();

    // Se le due query sono andate a buon fine restituisce lo stesso bene in
    // input ma con gli ID aggiornati, altrimenti false.
    if($aggiunto_dati_generici and $aggiunto_dati_specifici)
    {
      return $bene;
    }
    else
    {
      return false;
    }
  }

  /**
   * Aggiunge nella lista della spesa un bene venduto al dettaglio.
   * @param  Bene_venduto_al_dettaglio $bene Il bene che si vuole inserire nella
   * lista della spesa.
   * @return Bene_venduto_al_dettaglio|false Lo stesso oggetto passato in input ma con gli ID aggiornati.
   * Se fallisce una delle due registrazioni (generica o specifica) restituisce
   * false.
   */
  public function aggiungi_al_dettaglio(Bene_venduto_al_dettaglio $bene)
  {
    // Prepara i dati specifici per i beni venduti al peso.
    $dati_specifici = array(
      'quantita' => $bene->get_quantità(),
      'quantita_nel_carrello' => $bene->get_quantità_nel_carrello()
    );

    // Esegue la query e prende l'ID specifico autogenerato dal database.
    $aggiunto_dati_specifici = $this->db->insert($this->db->dbprefix('venduti_al_dettaglio'), $dati_specifici, true);
    $bene->set_id_specifico($this->db->insert_id());

    // Ottiene l'istruzione SQL per il debug.
    $SQL_specifica = $this->db->last_query();

    // Prepara i dati generici comuni a tutti i beni.
    $dati_generici = array(
      'id_al_peso' => null,
      'id_al_dettaglio' => $bene->get_id_specifico(),
      'nome_comune' => $bene->get_nome_comune(),
      'marca' => $bene->get_marca(),
      'prezzo' => $bene->get_prezzo(),
      'nota' => $bene->get_nota()
    );

    // Esegue la query e prende l'ID generico autogenerato dal database.
    $aggiunto_dati_generici = $this->db->insert($this->db->dbprefix('lista'), $dati_generici, true);
    $bene->set_id($this->db->insert_id());

    // Ottiene l'istruzione SQL per il debug.
    $SQL_generica = $this->db->last_query();

    // Se le due query sono andate a buon fine restituisce lo stesso bene in
    // input ma con gli ID aggiornati, altrimenti false.
    if($aggiunto_dati_generici and $aggiunto_dati_specifici)
    {
      return $bene;
    }
    else
    {
      return false;
    }
  }

  /**
   * Aggiunge un bene venduto al peso al carrello per la quantità specificata.
   * @param  int    $id_specifico          L'ID specifico del bene.
   * @param  int    $quantità_nel_carrello La quantità nel carrello da specificare.
   */
  public function aggiungi_al_carrello_al_peso(int $id_specifico, int $quantità_nel_carrello)
  {
    $this->db->set('quantita_nel_carrello', $quantità_nel_carrello);
    $this->db->where('id', $id_specifico);
    $this->db->update($this->db->dbprefix('venduti_al_peso'));
  }

  /**
   * Aggiunge un bene venduto al dettaglio al carrello per la quantità specificata.
   * @param  int    $id_specifico          L'ID specifico del bene.
   * @param  int    $quantità_nel_carrello La quantità nel carrello da specificare.
   */
  public function aggiungi_al_carrello_al_dettaglio(int $id_specifico, int $quantità_nel_carrello)
  {
    $this->db->set('quantita_nel_carrello', $quantità_nel_carrello);
    $this->db->where('id', $id_specifico);
    $this->db->update($this->db->dbprefix('venduti_al_dettaglio'));
  }

  /**
   * Modifica nella lista della spesa un bene venduto al peso.
   * @param  Bene_venduto_al_peso $bene Il bene che si vuole modificare nel database.
   * @return bool true se la query ha avuto successo, altrimenti false.
   */
  public function modifica_al_peso(Bene_venduto_al_peso $bene)
  {
    // Prepara i dati specifici da modificare.

    if($this->quantità_nel_carrello_del_bene_al_peso($bene->get_id_specifico()) > 0)
    {
      // Se la quantità nel carrello è già presente, sostituisce quella presa
      // dal database
      $dati_specifici = array(
        'id' => $bene->get_id_specifico(),
        'quantita' => $bene->get_quantità(),
        'unita_di_misura' => $bene->get_misura(),
        'quantita_nel_carrello' => $this->quantità_nel_carrello_del_bene_al_peso($bene->get_id_specifico())
      );
    }
    else
    {
      // Se la quantità nel carrello non è presente, la imposta a 0.
      $dati_specifici = array(
        'id' => $bene->get_id_specifico(),
        'quantita' => $bene->get_quantità(),
        'unita_di_misura' => $bene->get_misura(),
        'quantita_nel_carrello' => 0.0
      );
    }

    // Esegue la query che modifica i dati specifici.

    $this->db->set($dati_specifici);
    $this->db->where('id', $bene->get_id_specifico());
    $modificato_dati_specifici = $this->db->update($this->db->dbprefix('venduti_al_peso'));

    // Utilizzato a scopo di debug.
    // $query_specifica = $this->db->last_query();

    // Prepara i dati generici da modificare.
    $dati_generici = array(
      'id' => $bene->get_id(),
      'id_al_peso' => $bene->get_id_specifico(),
      'id_al_dettaglio' => null,
      'nome_comune' => $bene->get_nome_comune(),
      'marca' => $bene->get_marca(),
      'prezzo' => $bene->get_prezzo(),
      'nota' => $bene->get_nota()
    );

    // Esegue la query che modifica i dati generici.
    $modificato_dati_generici = $this->db->replace($this->db->dbprefix('lista'), $dati_generici);
    $query_generica = $this->db->last_query();

    // Se le due query sono andate a buon fine restituisce true, altrimenti false.
    if($modificato_dati_specifici and $modificato_dati_generici)
      return true;
    else
      return false;
  }

  /**
   * Modifica nella lista della spesa un bene venduto al dettaglio.
   * @param  Bene_venduto_al_dettaglio $bene Il bene che si vuole modificare nel database.
   * @return bool true se la query ha avuto successo, altrimenti false.
   */
  public function modifica_al_dettaglio(Bene_venduto_al_dettaglio $bene)
  {
    // Prepara i dati specifici da modificare.

    if($this->quantità_nel_carrello_del_bene_al_dettaglio($bene->get_id_specifico()) > 0)
    {
      // Se la quantità nel carrello è già presente, sostituisce quella presa
      // dal database.
      $dati_specifici = array(
        'id' => $bene->get_id_specifico(),
        'quantita' => $bene->get_quantità(),
        'quantita_nel_carrello' => $this->quantità_nel_carrello_del_bene_al_dettaglio($bene->get_id_specifico())
      );
    }
    else
    {
      // Se la quantità nel carrello non è presente, la imposta a 0.
      $dati_specifici = array(
        'id' => $bene->get_id_specifico(),
        'quantita' => $bene->get_quantità(),
        'quantita_nel_carrello' => 0
      );
    }

    // Esegue la query che modifica i dati specifici.
    $modificato_dati_specifici = $this->db->replace($this->db->dbprefix('venduti_al_dettaglio'), $dati_specifici);

    // Utilizzato a scopi di debug.
    // $query_specifica = $this->db->last_query();

    // Prepara i dati generici da modificare.
    $dati_generici = array(
      'id' => $bene->get_id(),
      'id_al_peso' => null,
      'id_al_dettaglio' => $bene->get_id_specifico(),
      'nome_comune' => $bene->get_nome_comune(),
      'marca' => $bene->get_marca(),
      'prezzo' => $bene->get_prezzo(),
      'nota' => $bene->get_nota()
    );

    // Esegue la query che modifica i dati generici.
    $modificato_dati_generici = $this->db->replace($this->db->dbprefix('lista'), $dati_generici);
    $query_generica = $this->db->last_query();

    // Se le due query sono andate a buon fine restituisce true, altrimenti false.
    if($modificato_dati_specifici and $modificato_dati_generici)
      return true;
    else
      return false;
  }

  /**
   * Elimina un bene venduto al peso dalla lista della spesa.
   * @param  Bene_venduto_al_peso $bene Il bene da eliminare dalla lista.
   * @return bool true se è stato eliminato correttamente, altrimenti false.
   */
  public function elimina_al_peso(Bene_venduto_al_peso $bene)
  {
    $this->db->where('id', $bene->get_id_specifico());
    $eliminati_dati_specifici = (bool) $this->db->delete($this->db->dbprefix('venduti_al_peso'));

    $this->db->where('id', $bene->get_id());
    $eliminati_dati_generici = (bool) $this->db->delete($this->db->dbprefix('lista'));

    if($eliminati_dati_specifici and $eliminati_dati_generici)
      return true;
    else
      return false;
  }

  /**
   * Elimina un bene venduto al dettaglio dalla lista della spesa.
   * @param  Bene_venduto_al_dettaglio $bene Il bene da eliminare dalla lista.
   * @return bool true se è stato eliminato correttamente, altrimenti false.
   */
  public function elimina_al_dettaglio(Bene_venduto_al_dettaglio $bene)
  {
    $this->db->where('id', $bene->get_id_specifico());
    $eliminati_dati_specifici = (bool) $this->db->delete($this->db->dbprefix('venduti_al_dettaglio'));

    $this->db->where('id', $bene->get_id());
    $eliminati_dati_generici = (bool) $this->db->delete($this->db->dbprefix('lista'));

    if($eliminati_dati_specifici and $eliminati_dati_generici)
      return true;
    else
      return false;
  }

  /**
   * Prende dal database l'intera lista della spesa
   * @return array eterogeneo composto da Bene_venduto_al_dettaglio e/o Bene_venduto_al_peso
   */
  public function lista_intera()
  {
    // Carica le tabelle.
    $lista_generica = $this->db->get($this->db->dbprefix('lista'));
    $lista_al_peso = $this->db->get($this->db->dbprefix('venduti_al_peso'));
    $lista_al_dettaglio = $this->db->get($this->db->dbprefix('venduti_al_dettaglio'));

    $lista_completa = array();
    if($lista_generica != false)
    {
      foreach($lista_generica->result_array() as $bene_incompleto)
      {
        // Elabora solo i beni venduti al peso.
        if($bene_incompleto['id_al_peso'] != null)
        {
          $bene = new Bene_venduto_al_peso();

          // Crea la parte generica del bene.
          $bene->set_id($bene_incompleto['id'])
               ->set_id_specifico($bene_incompleto['id_al_peso'])
               ->set_nome_comune($bene_incompleto['nome_comune'])
               ->set_marca($bene_incompleto['marca'])
               ->set_prezzo($bene_incompleto['prezzo'])
               ->set_nota($bene_incompleto['nota']);

          foreach($lista_al_peso->result_array() as $parte_specifica_del_bene)
          {
            // Cerca quale è la parte specifica tramite l'ID specifico.
            if($parte_specifica_del_bene['id'] == $bene->get_id_specifico())
            {
              // Aggiunge la parte specifica del bene.
              $bene->set_quantità($parte_specifica_del_bene['quantita'])
                   ->set_misura($parte_specifica_del_bene['unita_di_misura'])
                   ->set_quantità_nel_carrello((float)$parte_specifica_del_bene['quantita_nel_carrello']);
            }
          }

          // Aggiunge il bene completo nella lista completa.
          $lista_completa[] = $bene;
        }

        // Elabora solo i beni venduti al dettaglio.
        if($bene_incompleto['id_al_dettaglio'] != null)
        {
          $bene = new Bene_venduto_al_dettaglio();

          // Crea la parte generica del bene.
          $bene->set_id($bene_incompleto['id'])
               ->set_id_specifico($bene_incompleto['id_al_dettaglio'])
               ->set_nome_comune($bene_incompleto['nome_comune'])
               ->set_marca($bene_incompleto['marca'])
               ->set_nota($bene_incompleto['nota'])
               ->set_prezzo($bene_incompleto['prezzo']);

          foreach($lista_al_dettaglio->result_array() as $parte_specifica_del_bene)
          {
            // Cerca quale è la parte specifica tramite l'ID specifico.
            if($parte_specifica_del_bene['id'] == $bene->get_id_specifico())
            {
              // Aggiunge la parte specifica del bene.
              $bene->set_quantità($parte_specifica_del_bene['quantita'])
                   ->set_quantità_nel_carrello($parte_specifica_del_bene['quantita_nel_carrello']);
            }
          }
        $lista_completa[] = $bene;
        }
      }
    }
    // Ordina la lista per ID generico e lo restituisce.
    $funzione_ordinatrice = 'Spesa::ordina_beni_per_id';
    usort($lista_completa, $funzione_ordinatrice);
    return $lista_completa;
  }

  /**
   * Calcola il totale da spendere per l'intera lista della spesa-
   * @return float Il totale da spendere.
   */
  public function totale()
  {
    $totale = 0.0;
    $lista = $this->lista_intera();
    foreach($lista as $bene)
    {
      if($bene->get_quantità_nel_carrello() > 0)
        $totale += $bene->get_prezzo() * $bene->get_quantità_nel_carrello();
      else
        $totale += $bene->get_prezzo() * $bene->get_quantità();
    }
    return $totale;
  }

  /**
   * Compara due beni per ID generico.
   * @param  object $bene_a Il primo bene.
   * @param  object $bene_b Il secondo bene.
   * @return int Restituisce 1 se il primo ha un ID maggiore del secondo, 0 se
   * hanno gli ID uguali (cosa che non accadrà mai), -1 se il primo ha un ID
   * minore del secondo.
   */
  public static function ordina_beni_per_id($bene_a, $bene_b)
  {
    if(is_object($bene_a) and is_object($bene_b))
    {
      if($bene_a->get_id() > $bene_b->get_id())
        return 1;
      elseif($bene_a->get_id() == $bene_b->get_id())
        return 0;
      else
        return -1;
    }
    else
    {
      return null;
    }
  }

  /**
   * Svuota la lista della spesa.
   */
  public function svuota_lista()
  {
    $this->db->truncate($this->db->dbprefix('lista'));
    $this->db->truncate($this->db->dbprefix('venduti_al_dettaglio'));
    $this->db->truncate($this->db->dbprefix('venduti_al_peso'));
  }

  /**
   * Serve a ottenere la quantità nel carrello prima di modificare il record del
   * bene, altrimenti la quantità nel carrello verrebbe azzerata.
   * @param  int    $id_specifico L'ID specifico del bene di cui si vuole sapere la quantità nel carrello.
   * @return int|null    La quantità nel carrello.
   */
  public function quantità_nel_carrello_del_bene_al_dettaglio(int $id_specifico)
  {
    $this->db->select('quantita_nel_carrello');
    $this->db->where('id', $id_specifico);
    $query = $this->db->get($this->db->dbprefix('venduti_al_dettaglio'));
    $risultato = $query->result_array();
    foreach($risultato as $quantità_nel_carrello)
    {
      return (int)$quantità_nel_carrello['quantita_nel_carrello'];
    }
  }

  /**
   * Serve a ottenere la quantità nel carrello prima di modificare il record del
   * bene, altrimenti la quantità nel carrello verrebbe azzerata.
   * @param  int    $id_specifico L'ID specifico del bene di cui si vuole sapere la quantità nel carrello.
   * @return int|null    La quantità nel carrello.
   */
  public function quantità_nel_carrello_del_bene_al_peso(int $id_specifico)
  {
    $this->db->select('quantita_nel_carrello');
    $this->db->where('id', $id_specifico);
    $query = $this->db->get($this->db->dbprefix('venduti_al_peso'));
    $risultato = $query->result_array();
    foreach($risultato as $quantità_nel_carrello)
    {
      return (float)$quantità_nel_carrello['quantita_nel_carrello'];
    }
  }
}
