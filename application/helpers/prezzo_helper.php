<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('formatta_prezzo'))
{
  function formatta_prezzo(float $prezzo)
  {
    return number_format($prezzo, 2, ",", ".");
  }
}
